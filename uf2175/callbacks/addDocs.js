/*
Código para pruebas de rendimiento de la base de datos:
la función addDocs() añade 1000 documentos a la base de datos
para ver cómo se comportan los _temp_views
*/

const database = 'http://localhost:5984/garbage/'

var ajaxDefault = {
	type: 'post',
	url: database,
	dataType: 'json',
	contentType: "application/json; charset=utf-8",
	success: function(data){ console.log('Success!!', data)},
	failure: function(errMsg){ console.log('Error!!', errMsg) }
}


function addDocs(){
	let n = 0, min = 65, max = 122
	while ( n < 1000 ) {		
		let l1 = Math.random() * (max - min) + min
		let l2 = Math.random() * (max - min) + min
		let l3 = Math.random() * (max - min) + min
		let l4 = Math.random() * (max - min) + min
		let l5 = Math.random() * (max - min) + min
		let palabra = String.fromCharCode(l1,l2,l3,l4,l5)
		ajaxDefault.data = JSON.stringify({ numero: n, palabra: palabra })
		$.ajax(ajaxDefault)
		n++
	}
}

$(document).ready( function(){			
	console.log('jQuery document ready!!')
	
});
