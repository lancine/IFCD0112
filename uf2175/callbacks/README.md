<h2>vistas temporales en CouchDB y funciones callback</h2>
<p>
	en este ejemplo introducimos la utilidad de las funciones callback a propósito de la creación de views temporales en CouchDB;	
</p>
<p>
	preparamos una función general que se encargue de generar vistas temporales para ordenar los listados y llamamos a esa función con diferentes callbacks para producir diferentes resultados;
</p>
