const database = 'http://localhost:5984/garbage/'
// establezco los valores por defecto para la función $.ajax()
// de manera que sólo cambio los calores necesarios antes de cada llamada
var ajaxDefault = {
	type: 'get',
	url: database,
	dataType: 'json',
	contentType: "application/json; charset=utf-8",
	success: function(data){ console.log('Success!!', data)},
	failure: function(errMsg){ console.log('Error!!', errMsg) }
}

// preparo la funcion para todas las vistas temporales
// se le pasan los valores de filtro y de ordenación y el callback
function tempView(attr1,attr2,callback){
	let view = `{
		"map": "function(doc){ if( doc.${attr1} && doc.${attr2} ){ emit(doc.${attr1}, doc.${attr2}) }}"
	}`
	ajaxDefault.type = 'post'
	ajaxDefault.success = callback
	ajaxDefault.data = view
	ajaxDefault.url = database + '_temp_view'
	$.ajax(ajaxDefault)	
}

function listadoApellidos(data){
	$('#result').html('<hr>')
	$.each(data.rows, function(idx, row){ 
		$('#result').append('<li>' + row.key + '</li>')		
	});
	$('#selectAlumnos').hide()
	$('#result').show()
}

function selectApellidos(data){	
	$('#selectAlumnos').html('')
	$.each( data.rows, function(idx, row){ 		
		let str = `<option value="${row.id}">${row.key} - ${row.value} </option>`		
		$('#selectAlumnos').append(str)		
		$('#result').hide()
		$('#selectAlumnos').show()
	})

}

$(document).ajaxStart(function(){
    $("#wait").css("display", "block");
});

$(document).ajaxComplete(function(){
    $("#wait").css("display", "none");
});

$(document).ready( function(){			
	console.log('jQuery document ready!!')
});
