const database = 'http://localhost:5984/garbage/'

function getAlumnos(){
	$.ajax({
	  type: "get",
	  url: database + "_all_docs?include_docs=true",
	  data: null,
	  success: listaAlumnos,
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	});		
}

function listaAlumnos(data){	
	// limpio el DIV
	$('#listaAlumnos').html('')	
	// recorremos los documentos
	$.each( data.rows, function(idx, row){ 
		// filtramos los documentos que tengan class == garbage
		// en lugar de un view en la base de datos
		if ( row.doc.class == 'garbage' ) {
			// lo añado a la lista
			let html = `
				<button onclick="getAlumno('${row.id}')">
				${row.doc.nombre} ${row.doc.apellidos}
				</button>
				`
			$('#listaAlumnos').append('<li>' + html + '</li>')
		}
	});
}

function getAlumno(id){	
	$.ajax({
	  type: "get",
	  url: database + id,
	  data: null,
	  success: editAlumno,
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	});			
}

function editAlumno(alumno){
	$('#id').val(alumno._id)
	$('#rev').val(alumno._rev)
	$('#gitUser').val(alumno.gitUser)
	$('#apellidos').val(alumno.apellidos)
	$('#nombre').val(alumno.nombre)
	$('#gitProject').val(alumno.gitProject)
	$('#telefono').val(alumno.telefono)
}

function saveAlumno(action){	
	// TODO: falta revisar campos obligatorios
	// establezco url y método por defecto para hacer desde aquí CUD
	// La R de CRUD la hace getAlumno()
	let url = database
	let method = 'post'
	// preparo el objeto a enviar:	
	let alumno = {
		_id: $('#id').val(),
		_rev: $('#rev').val(),
		class: 'garbage',
		nombre: $('#nombre').val(),
		apellidos: $('#apellidos').val(),
		gitUser: $('#gitUser').val(),
		gitProject: $('#gitProject').val(),
		telefono: $('#telefono').val()
	}
	// la C de CRUD
	// si es creación de nuevo documento no enviamos _id y _rev
	if (action == 'new') { 
		delete alumno['_id']
		delete alumno['_rev']		
	}
	// la D de CRUD
	if (action == 'delete'){
		method = 'delete'
		url += alumno._id + '?rev=' + alumno._rev
		alumno = null		
	}

	// y envío por Ajax la petición tal como ha quedado después de los if()
	$.ajax({
	  type: method,
	  url: url,
	  data: JSON.stringify(alumno),
	  success: function(errMsg){ console.log(errMsg); getAlumnos() },
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	});	

}

$(document).ready( function(){
	getAlumnos();
});
