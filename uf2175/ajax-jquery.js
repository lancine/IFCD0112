/*

este ejercicio es una introducción al uso de Ajax mediante jQuery

a) comentar el paso de parámetros entre funciones en forma de objeto,
así la función sabe qué usar y cómo, aunque los parámetros se pasen desordenados

*/


function getDataBases(){
	$.ajax({
	  type: "get",
	  url: "http://localhost:5984/_all_dbs",
	  data: null,
	  success: function(data){ console.log(data) },
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	});	
}


// comentar $(document).ready() en el entorno de jQuery
$(document).ready( function(){
	getDataBases();
});


