<h2>ejercicios sobre CouchDb y Ajax</h2>
<p>
	en esta Unidad Formativa (Uf2175) estamos haciendo una primera aproximación al uso de bases de datos NoSql de cara a una comparación posterior con los RDBMS
</p>
<p>
	en esta carpeta tenemos:
</p>

<ol type="a">
	<li>ejemplo de llamada Ajax con ES6 sobre un servidor CouchDB<br>
		<p>archivos: <code>index.html</code> y <code>es6-couchdb.js</code></p>
		<p>ejecutar con lite-server sobre esta carpeta</p>
	</li>
	<li>ejemplo básico de jQuery Ajax sobre un servidor CouchDB<br>
		<p>archivo: <code>ajax-jquery.js</code></p>
	</li>	
	<li>ejemplo <code>CRUD</code> desde <code>jQuery</code> sobre un servidor <code>CouchDB</code><br>
		<p>archivos: <code>alumnos.html</code> y <code>alumnos.js</code></p>
		<p>ejecutar con lite-server desde esta carpeta en http://localhost:3000/alumnos.html</p>
	</li>	
	<li>idéntica aplicación a la anterior pero ejecutada como desktop con <code>NWJS</code><br>
		<p>archivos: <code>alumnos.html</code>, <code>alumnos.js</code> y <code>package.json</code></p>
		<p>ejecutar desde esta carpeta con <code>~/nwjs/nw .</code></p>
	</li>		
</ol>