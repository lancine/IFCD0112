var express = require('express')
var app = express()
var request = require('request')
var bodyParser = require('body-parser')
var mysql      = require('mysql')
var connection = mysql.createConnection({
  host     : 'localhost',
  database : 'test'
})
var port = 3333
connection.connect();
app.use(require('body-parser').json());    

app.get('/', function (req, res) {
  let msg = `
    <h2>middleWare Express - Mysql</h2>
    <h3>uso</h3>
    <li>GET http://localhost/alumnos</li>
  `
  res.send(msg);
});

app.get('/alumnos', function (req, res) {    
  // curl http://localhost:3333/alumnos
  connection.query('SELECT * FROM alumnos;', function (error, results, fields) {    
    res.send(results)
  });  
});

app.get('/alumno/:id', (req,res) => {
  let sql = `
    select * from alumnos 
    where id=${req.params.id}
  `
  connection.query(sql, function (error, results, fields) {        
    res.send(results[0])
  });    
})

app.post('/alumno', (req,res) => {   
  // curl 
  // -X POST http://localhost:3333/alumno 
  // -d '{"nombre": "Juan","apellidos": "Pérez Gómez"}' 
  // -H "Content-Type: application/json"  
  let alumno = req.body
  let sql = `
    insert into alumnos (apellidos,nombre)
    values ( "${alumno.apellidos}", "${alumno.nombre}" )
  `
  connection.query(sql, function (error, results, fields) {        
    res.send(results)
  });
});



app.listen(port, function () {
  console.log('mysql middleWare escuchando en puerto ' + port);
});


