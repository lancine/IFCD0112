import { TestBed, inject } from '@angular/core/testing';

import { Ifcd0112Service } from './ifcd0112.service';

describe('Ifcd0112Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Ifcd0112Service]
    });
  });

  it('should be created', inject([Ifcd0112Service], (service: Ifcd0112Service) => {
    expect(service).toBeTruthy();
  }));
});
