import { Component, OnInit } from '@angular/core';
import { Customer } from '../ifcd0112.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms'
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['../app.component.css'],
  providers: [ Customer ]
})
export class CustomersComponent implements OnInit {
  private form: FormGroup;  
  constructor( private customer: Customer, private builder: FormBuilder){ 
    this.form = builder.group(customer.getForm())    
  }
  ngOnInit(){
  }
}
