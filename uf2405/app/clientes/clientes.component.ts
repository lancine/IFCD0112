import { Component, OnInit } from '@angular/core';
import { Ifcd0112Service } from '../ifcd0112.service';
import { Modal } from 'ngx-modal';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['../app.component.css'],
  providers: [ Ifcd0112Service ]
})
export class ClientesComponent implements OnInit {

  constructor( private serv: Ifcd0112Service) { }

  ngOnInit() {
  }

}
