import { Component, OnInit, Input } from '@angular/core';
import { Posts } from '../ifcd0112.service'

@Component({
  selector: 'app-pruebas',
  templateUrl: './pruebas.component.html',
  styleUrls: ['../app.component.css'],
  providers: [ Posts ]
})
export class PruebasComponent implements OnInit{    
  constructor( private posts: Posts ){ /*...*/ }
  ngOnInit(){ /*...*/ }

}


@Component({
  selector: 'detail',
  templateUrl: './detail.component.html',
  styleUrls: ['../app.component.css'],
  providers: [  ]
})
export class DetailComponent implements OnInit{  
  @Input() post: object;
  constructor(  ){ /*...*/ }
  ngOnInit(){ /*...*/ }

}

