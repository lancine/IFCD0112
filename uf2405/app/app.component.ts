import { Component, Input } from '@angular/core';
import { Ifcd0112Service } from './ifcd0112.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',  
  styleUrls: ['./app.component.css'],
  providers: [ Ifcd0112Service ]
  
})
export class AppComponent { 
  constructor( private s: Ifcd0112Service ){}
}

