import { Component, OnInit } from '@angular/core';
import { Uploads } from '../ifcd0112.service'
import { FormBuilder, FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['../app.component.css'],
  providers: [ Uploads ]
})
export class UploadsComponent implements OnInit {
  private form: FormGroup; 
  constructor( private uploads: Uploads, private builder: FormBuilder) {   }
  ngOnInit() { 
    this.form = this.builder.group(this.uploads.getForm())      
  }

}
