import { Component, OnInit } from '@angular/core';
import { Ifcd0112Service, FormAlumnos } from '../ifcd0112.service';
import {
  FormBuilder,  // para FormBuilder.group()
  FormControl,  // para función de validación de apellidos
  FormGroup,    // para [formGroup]="formClientes" en el template
  Validators    // para Valodator.compose, required, etc.
  } from '@angular/forms';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['../app.component.css'],
  providers: [ Ifcd0112Service ]
})
export class AlumnosComponent implements OnInit {  
  private formAlumno: FormGroup;  
  private expandir: string = 'glyphicon glyphicon-resize-full marco';
  private comprimir: string = 'glyphicon glyphicon-resize-small marco';  

  constructor(private serv: Ifcd0112Service, private fb: FormBuilder) {
    // construir un form a partir de una clase: 
    this.formAlumno = fb.group(new FormAlumnos());
  }
  
  ngOnInit(){  }

  addTask(title, fecha){
    if(!this.formAlumno.value.tasks){ this.formAlumno.value.tasks = [] }
    this.formAlumno.value.tasks.push({titulo: title, fecha: fecha})  
  }
  deleteTask(i){
    this.formAlumno.value.tasks.splice(i, 1)
  }
  sendQuery(query){ 
    // una pizarra de pruebas para enviar querys a firebase
    this.serv.getQuery('/alumnos/', JSON.parse(query) ); }
  
  

}
