import { Component, OnInit, ViewChild } from '@angular/core';
import { Ifcd0112Service } from '../ifcd0112.service';
import { Modal } from 'ngx-modal';

@Component({
  selector: 'app-domingo',
  templateUrl: './domingo.component.html',
  styleUrls: ['./domingo.component.css'],
  providers: [ Ifcd0112Service ]
})
export class DomingoComponent implements OnInit {  
  @ViewChild(Modal) private modal: Modal;  
  constructor( private s: Ifcd0112Service ){         
  }

  ngOnInit(){   } 

}
