import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-modal';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AppComponent } from './app.component';
import { ClientesComponent } from './clientes/clientes.component';
import { AlumnosComponent } from './alumnos/alumnos.component';
import { DomingoComponent } from './domingo/domingo.component';
import { CustomersComponent } from './customers/customers.component'
import { RouterModule, Routes } from '@angular/router';
import { PruebasComponent, DetailComponent } from './pruebas/pruebas.component';
import { LoginComponent } from './login/login.component';
import { UploadsComponent } from './uploads/uploads.component';
import { enableProdMode } from '@angular/core';
enableProdMode();

const firebaseProject: object = {
  apiKey: "AIzaSyB6Q5w_P2EaOoVq9AjFwEXQ7qRO2g1i5Gw",
  authDomain: "ifcd0112.firebaseapp.com",
  databaseURL: "https://ifcd0112.firebaseio.com",
  projectId: "ifcd0112",
  storageBucket: "ifcd0112.appspot.com",
  messagingSenderId: "303590434235"
}

const appRoutes: Routes = [  
  { path: 'clientes', component: ClientesComponent },
  { path: 'alumnos', component: AlumnosComponent },
  { path: 'login', component: LoginComponent },  
  { path: 'customers', component: CustomersComponent },  
  { path: 'testing', component: PruebasComponent },  
  { path: 'domingo', component: DomingoComponent },  
  { path: 'uploads', component: UploadsComponent },  
  { path: '**', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
]

// declaración del módulo:
@NgModule({
  declarations: [
    AppComponent, 
    ClientesComponent,
    AlumnosComponent,
    DomingoComponent,
    PruebasComponent, DetailComponent,
    CustomersComponent,
    LoginComponent,
    UploadsComponent,
    
  ],
  imports: [
    BrowserModule,  
    FormsModule,
    ReactiveFormsModule,
    ModalModule,    
    AngularFireModule.initializeApp(firebaseProject),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot(appRoutes),  
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
