import { Component, OnInit, ViewChild } from '@angular/core';
import { Login } from '../ifcd0112.service';
import { Modal } from 'ngx-modal';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../app.component.css'],
  providers: [ Login ]
})
export class LoginComponent implements OnInit {  
  @ViewChild(Modal) private modal: Modal;  
  constructor( private login: Login ){         
  }

  ngOnInit(){   } 

}
