import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

// Posts
@Injectable()
export class Posts {
  private data: FirebaseListObservable<any[]>; 
  private dbName: string = '/posts/'
  constructor(private ngDb: AngularFireDatabase){
    this.data = ngDb.list(this.dbName, {query: { orderByChild: 'titulo'}});  
  }
}

// ejemplo Chat
@Injectable()
export class Chat {
  private data: FirebaseListObservable<any[]>; 
  private dbName: string = '/chat/'
  public form: FormGroup;
  constructor(    
    public ngAuth: AngularFireAuth,
    private ngDb: AngularFireDatabase,
    private builder: FormBuilder
  ){
    this.data = ngDb.list(this.dbName, {query: { orderByChild: 'dateInverse'}});     
    this.form = builder.group(this.getForm())
  }
  getForm(){
    return {          
      autor: [''],
      displayName: [''],
      mensaje: ['', Validators.required],      
      dateEdit: [''],    
      timeEdit: [''],    
      dateInverse: ['']   
    }
  }

  setAutoValues(){
    this.form.value.dateInverse = -1 * new Date().getTime();
    this.form.value.dateEdit = new Date().toLocaleDateString();
    this.form.value.timeEdit = new Date().toLocaleTimeString();
    this.form.value.autor = this.ngAuth.auth.currentUser.email;
    this.form.value.displayName = this.ngAuth.auth.currentUser.displayName;
  }
  
  sendMensaje(){
    this.setAutoValues();    
    this.data.push(this.form.value);
    this.form.reset()
  }

}


// ejemplo gestión de adjuntos 
@Injectable()
export class Uploads {
  public files: Array<any> = [];
  private data: FirebaseListObservable<any[]>; 
  private dbName: string = '/uploads/';
  constructor( 
    private ngAuth: AngularFireAuth,
    private ngDb: AngularFireDatabase){
    this.data = ngDb.list(this.dbName, {query: { orderByChild: 'dateEdit'}});     
  }

  getForm(){
    return {    
      $key: [''],
      titulo: ['', Validators.required],
      descripcion: ['', Validators.required],
      editBy: [''],
      dateEdit: [''], 
      uid: [''],
      files: [ [] ]      
    }
  }

  addFiles(files){    
    if (files){
      for (var index = 0; index < files.length; index++) {        
        this.files.push(files[index])
      }       
    }
  } 

  deleteSession(key){        
    this.ngDb.object(this.dbName + key).remove();
  }

  saveForm(value){    
    value.dateEdit = new Date().toISOString();
    value.editBy = this.ngAuth.auth.currentUser.email;    
    value.uid = this.ngAuth.auth.currentUser.uid;    
    let path = this.dbName + value.$key;
    if ( value.$key ){      
      delete value.$key;     
      this.ngDb.object(path).set(value);
    }
    else {
      delete value.$key;
      this.data.push(value);
    }
  }

  saveFiles(value){      
    this.files.forEach((file) => {
      this.uploadFile(file).then( url => {
        value.files.push({ name: file.name, url: url })          
      })
    })    
    this.files = [];
  }

  uploadFile(file: File): Promise<string> {    
    let path = this.folderName()
    return new Promise( ( resolve, reject ) => {
      if ( path ){
        let ref = firebase.storage().ref(path);
        let task: firebase.storage.UploadTask = ref.child(file.name).put(file);
        task.on(firebase.storage.TaskEvent.STATE_CHANGED,
          (data) => {  },
          (error) => {  },
          () => {                    
            resolve(task.snapshot.downloadURL);          
          }
        )
      }
      else { resolve('upload error')}
    })
  }

  fileRemove(file, value, index){        
    value.files.splice(index, 1)
    // desactivo el borrado real de Storage, para evitar urls inexistentes
    // let ref = firebase.storage().ref( this.sanitizeFolderName(value.coleccion) );    
    // ref.child(file.name).delete().then( nada => {})
  }

  folderName(): string {
    let coleccion = new Date().toISOString().split('T')[0];
    return this.ngAuth.auth.currentUser.uid + '/' + coleccion + '/';
  }


}

// Formulario de Alumnos:
export class FormAlumnos {
  $key: Array<any> = [''];
  apellidos: Array<any> = ['', Validators.required];
  nombre: Array<any> = ['', Validators.required];
  direccion: Array<any> = [''];
  email: Array<any> = [''];
  telefono: Array<any> = [''];
  fechaAlta: Array<any> = [''];
  avatarUrl: Array<any> = [''];
  tasks: Array<any> = [[]];
  constructor(){  }
}

// ejemplo de inyectable que genera sus forms
@Injectable()
export class Customer {  
  public collName: string = '/customers/'
  private data: FirebaseListObservable<any[]>; 
  constructor( 
    private angularDb: AngularFireDatabase,
    public ngAuth: AngularFireAuth ){
    this.data = angularDb.list(this.collName,{ query: { orderByChild: 'lastname' }});     
  }  
  
  getUser(){
    let retVal = {  }
    if ( this.ngAuth.auth.currentUser ){
      retVal = { 
        uid: this.ngAuth.auth.currentUser.uid,
        email: this.ngAuth.auth.currentUser.email,
        displayName: this.ngAuth.auth.currentUser.displayName,
        photoURL: this.ngAuth.auth.currentUser.photoURL,
        phoneNumber: this.ngAuth.auth.currentUser.phoneNumber            
      }
    } 
    return retVal
  }

  getUserEmail(){
    return this.ngAuth.auth.currentUser.email
  }

  // devuelve el objeto que necesita FormBuilder.group()
  getForm(){
    return {    
      $key: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      address: [''],
      // sólo permite 5 letras
      code: ['', Validators.pattern('[A-Za-z]{5}')],
      // sólo permite 13 dígitos numéricos
      phone: ['', Validators.pattern('[0-9]{13}')],      
      editBy: [''],
      dateEdit: [''], 
      uid: ['']      
    }
  }

  saveCustomer(value){    
    // crea o actualiza si recibe o no un atributo $key con valor
    let path: string = this.collName + value.$key;    
    let hasKey: boolean = value.$key ? true : false;    
    delete value.$key; 
    // guardar usuario que modifica
    value.editBy = this.getUserEmail();
    // guardar fecha de modificación
    value.dateEdit = new Date().toLocaleString();
    // guardar uid: por si acaso
    value.uid = this.ngAuth.auth.currentUser.uid ;    
    if ( hasKey ) { this.angularDb.object( path ).update( value ) } 
    else { this.data.push(value) }    
  }

  deleteCustomer(key){     
    this.angularDb.object( this.collName + key ).remove()
  }
  
}

@Injectable()
export class Login {  
  private loggedIn: boolean = false;
  constructor( private ngAuth: AngularFireAuth){
    ngAuth.auth.onAuthStateChanged( (user) => { 
      if ( user ){ this.loggedIn = true  }      
      else { this.loggedIn = false }
    })
  }
  getUser(){
    let retVal = {}
    if ( this.ngAuth.auth.currentUser ){
      retVal = { 
        uid: this.ngAuth.auth.currentUser.uid,
        email: this.ngAuth.auth.currentUser.email,
        displayName: this.ngAuth.auth.currentUser.displayName,
        photoURL: this.ngAuth.auth.currentUser.photoURL,
        phoneNumber: this.ngAuth.auth.currentUser.phoneNumber            
      }
    } 
    return retVal
  }
  gmailLogin(){
		return this.ngAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())				
  }
}


@Injectable()
export class Ifcd0112Service {
  private coleccion: string = '/alumnos/';
  private carpetaStorage: string = '/alumnos/'
  public alumnos: FirebaseListObservable<any[]>; 
  public queryResults: FirebaseListObservable<any[]>;
  private defaultAvatar: any;  
  
  // Dependence Injection:
  constructor( private db: AngularFireDatabase, public aFAuth: AngularFireAuth ){    
      // inicializo this.alumnos a /alumnos/ ordernado por nombre
      this.alumnos = db.list(this.coleccion,{ query: { orderByChild: 'nombre' }});  
      // se inicializa también queryResults por si lo llama algún componente antes que a getQuery
      this.queryResults = this.alumnos;      
      // avatar por defecto:  
      firebase.storage().ref('/alumnos/').child('mujer.jpg').getDownloadURL()
      .then(url => this.defaultAvatar = url )          
       
  } 
  // dejo esta función porque tiene un new que no se puede poner en los templates
  // las demás Login, Logout, etc, se llaman desde los templates
  // mediante aFAuth.auth
 

  
   
  


// Cosas para gestión de firebase:
// pasar la colección y el query, los resultados estarán en queryResults
  getQuery(collection: string, query: object){    
    console.log(collection, query)
    this.queryResults = this.db.list(collection,query);  
  }
  // gestion de alumnos
  saveAlumno(form: any){
    let path: string = this.coleccion + form.$key;    
    let tieneKey: boolean = form.$key ? true : false;
    delete form.$key;
    if (tieneKey){ this.db.object(path).update(form) }
    else { this.alumnos.push(form)      
    }    
  }

  deleteAlumno(key: string){  
    console.log(this.coleccion + key)
    this.db.object(this.coleccion + key).remove();     
  }

  saveFile(file,form){   
    let procesoUpload: firebase.storage.UploadTask =    
        firebase.storage().ref(this.carpetaStorage).child(file.name).put(file);


    procesoUpload.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (data) => { console.log(data)  },
      (error) => { console.log(error) },
      () => {         
        let obj = form.value;
        obj.avatarUrl = procesoUpload.snapshot.downloadURL ;
        form.resetForm(obj);
      }
    )
  }

   
}
